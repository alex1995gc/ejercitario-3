<?php 
	echo "Version PHP: ".PHP_RELEASE_VERSION;
	echo '<br>';
	echo "ID de la versión de PHP: ".PHP_VERSION_ID;
	echo '<br>';
	echo "Valor máximo de enteros: ".PHP_INT_MAX;	
	echo '<br>';
	echo "Tamaño máximo del nombre de un archivo: ".PHP_MAXPATHLEN;
	echo '<br>';
	echo "Versión del Sistema Operativo: ".PHP_OS;
	echo '<br>';
	echo "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: ".PHP_EOL;
	echo '<br>';
	echo "Include Path por Defecto: ".DEFAULT_INCLUDE_PATH;
 ?>