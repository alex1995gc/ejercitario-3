<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ej3</title>
</head>
<body>
	<?php 
		const N = 25;
		echo 'N: '.N;
		echo '<br>';
		echo '<br>';
		echo '<table style="border:2px solid blue;">';
		echo '<tr>';
		echo	'<th>Números pares entre 1 y N</th>';
		echo '</tr>';
		for($i=1; $i<=N; $i++)
		{
			if ($i%2==0)
			{
				echo  '<tr>';
				echo	'<td style="border:1px solid blue;">'.$i.'</td>';
				echo  '</tr>';
			}
		}
		echo '</table>';

	 ?>
</body>
</html>